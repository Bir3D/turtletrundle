﻿using UnityEngine;
using System.Collections;

public class MouseManager : MonoBehaviour {

    void Update() {
        if(Input.GetMouseButtonDown(0)) {
            Vector3 mouseWorldPos3D = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mouseWorldPos3D.x, mouseWorldPos3D.y);
            Vector2 dir = Vector2.zero;
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, dir);

            if(hit.collider) {
                if (hit.collider.GetComponent<Rigidbody2D>()) {
                    hit.collider.GetComponent<Rigidbody2D>().gravityScale = 1;
                }
            }
        }
    }

    void FixedUpdate () {
    }

}
