﻿using UnityEngine;
using System.Collections;

public class PlatformLooper : MonoBehaviour {

    float cameraHeight;
    float cameraWidth;
    float platformMax;
    float platformMin;

    void Start() {
        GameObject[] platforms = GameObject.FindGameObjectsWithTag("Platform");
        foreach(GameObject platform in platforms) {
            CameraSize();
            Vector3 pos = platform.transform.position;
            pos.x = Random.Range(platformMin, platformMax);
            platform.transform.position = pos;

        }
        
    }

    void OnTriggerEnter2D(Collider2D collider) {
        Debug.Log("Triggered: " + collider.name);

        Vector3 pos = collider.transform.position;
        CameraSize();
        pos.x = Random.Range(platformMin, platformMax);
        pos.y += cameraHeight * 2;

        collider.transform.position = pos;
    }

    void CameraSize() {
        Camera cam = Camera.main;
        cameraHeight = 2f * cam.orthographicSize;
        cameraWidth = cameraHeight * cam.aspect;
        platformMax = cameraWidth / 2;
        platformMin = -cameraWidth / 2;
    }
}
