﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    Vector3 velocity = Vector3.zero;
    Vector3 lastPosition = Vector3.zero;
    public Vector3 gravity;
    public Vector3 jumpVelocity;
    public Vector3 sideSpeed;
    float speed;
    Vector3 yPos;
    bool didJump;
    BoxCollider2D bc;

	// Use this for initialization
	void Start () {
        bc = GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            didJump = true;
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.position -= sideSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D)) {
            transform.position += sideSpeed * Time.deltaTime;
        }

    }

    void FixedUpdate() {
        speed = transform.position.y - lastPosition.y;
        lastPosition = transform.position;

        velocity += gravity * Time.deltaTime;
        
        if (didJump == true) {
            didJump = false;
            velocity = jumpVelocity;
            
        }
        transform.position += velocity * Time.deltaTime;

    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (speed > 0)
            bc.enabled = false;
        else
            bc.enabled = true;
    }
}
